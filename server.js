const express = require('express')
const twig  = require('twig')

// const { led, button } = require('./gpio') // led(1, 'on')
const gpio = require('./gpio')            // gpio.led(1, 'on')

const app = express()
const path = require('path')
const port = 3000


// app.set('view engine', 'twig')
// app.set('views', __dirname + '/views')

app.engine('html', twig.__express)
app.set('views', path.join(__dirname, 'views'))
// app.set('view engine', 'twig')
app.set('view engine', 'html')
// app.set('view cache', false)
twig.cache(false)

app.use(express.static(path.join(__dirname, 'public')))

app.get('/', (req, res) => {
    // res.send('Hello World!') 그냥 출력
    res.render('index.html', { msg: 'hi'}) // 템플릿엔진을 통하여 저장 index.twig -> html
})

// button router
app.get('/button/on', (req, res) => {
    gpio.button('on')
    res.send('on all button')
})
app.get('/button/off', (req, res) => {
    gpio.button('off')
    res.send('off all button')
})
app.get('/button/:id/on', (req, res) => {
    let id = req.params.id
    console.log(id)
    gpio.button(id, 'on')
    res.send(`button ${id} now on`)
})
app.get('/button/:id/off', (req, res) => {
    let id = req.params.id
    console.log(id)
    gpio.button(id, 'off')
    res.send(`button ${id} now off`)
})

// led router
app.get('/led/on', (req, res) => {
    gpio.led('on')
    // todo:  gpio.led.on()
    res.send('on all led')
})
app.get('/led/off', (req, res) => {
    gpio.led('off')
    res.send('off all led')
})
app.get('/led/:id/on', (req, res) => {
    let id = req.params.id
    console.log(id)
    gpio.led(id, 'on')
    res.send(`led ${id} now on`)
})
app.get('/led/:id/off', (req, res) => {
    let id = req.params.id
    console.log(id)
    gpio.led(id, 'off')
    res.send(`led ${id} now off`)
})

// start web server
app.listen(port, () => {
    console.log(`Example app listening on port ${port}!`)
})

